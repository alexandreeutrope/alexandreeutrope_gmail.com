## Chaine de caractère, liste et dictionnaire.
s, l, d = 'hello', [22, 3.14, 'world'], {'age': 25, 'name': 'Alexandre'}
l, s, d

## Tuples
t = (22, 3.14, 'hello')
t

## Appeler les valeurs par indexes
s[0], s[1], s[0:4]

l[1], l[0] / 10 + 2**3

d = d['age'] + 25, d['name'] + " Eutrope"

STRING (MULTIPLE LINES)

# Utilisation chaine de caractère
Poeme = """Un soir, t’en souvient-il ? nous voguions en silence ;
On n’entendait au loin, sur l’onde et sous les cieux,
Que le bruit des rameurs qui frappaient en cadence
Tes flots harmonieux."""

# Somme de chaine de caractère
Poeme1 = "Un soir, t’en souvient-il ? nous voguions en silence\n"
Poeme1 += "On n’entendait au loin, sur l’onde et sous les cieux\n"
Poeme1 += "Que le bruit des rameurs qui frappaient en cadence Tes flots harmonieux"
Print Poeme1

# Lancé de dé
import random 
from random import randint
import numpy as np

nb_lancé = int (input ("Entrez le nombres de lancé : "))
i=0
liste=[]

for i in range (nb_lancé) :
    d = randint(1,6)
    liste.append (d)
    
somme = sum(liste)
moyenne = somme / nb_lancé 

print ("Les résultats obtenu sont :", liste)
print ("Leur valeur moyenne est : ", moyenne)
print ("Le dé est non pipé")

# STRING TEMPLATING WITH F-STRING
val = '54.4'  # the value

f"{val}"
# '54.4'

f"{val:%<9}"
# '54.4%%%%%'

f"{val:0>9}"
# '0000054.4'

f'=={s.upper():^16}=='  # **^** Forces the field to be centered
# ==     HELLO      ==

# MORE ON F-STRING

table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
for name, phone in table.items():
   print(f'{name:10} ==> {phone:10d}')
# Sjoerd     ==>       4127
# Jack       ==>       4098
# Dcab       ==>       7678

# Data structures have their own API
## string API
string_api = """
s.capitalize   s.format_map   s.isnumeric    s.maketrans    s.split
s.casefold     s.index        s.isprintable  s.partition    s.splitlines
s.center       s.isalnum      s.isspace      s.replace      s.startswith
s.count        s.isalpha      s.istitle      s.rfind        s.strip
s.encode       s.isascii      s.isupper      s.rindex       s.swapcase
s.endswith     s.isdecimal    s.join         s.rjust        s.title
s.expandtabs   s.isdigit      s.ljust        s.rpartition   s.translate
s.find         s.isidentifier s.lower        s.rsplit       s.upper
s.format       s.islower      s.lstrip       s.rstrip       s.zfill
"""

## list API
list_api = """
l.append  l.count   l.insert  l.reverse
l.clear   l.extend  l.pop     l.sort
l.copy    l.index   l.remove
"""

## dict API
dict_api = """
d.clear      d.get        d.pop        d.update
d.copy       d.items      d.popitem    d.values
d.fromkeys   d.keys       d.setdefault
"""

# Namespace

>>> dir()
['__builtins__', '__doc__', '__name__', '__package__', 'd', 'haiku3', 'l', 's',
 't']

>>> dir(__builtins__), len(dir(__builtins__))
(['ArithmeticError', 'AssertionError', 'AttributeError', '...',
 'TabError', 'True', 'ValueError', 'Warning', 'ZeroDivisionError',
 '__debug__', '__doc__', '__name__', '__package__', 'abs',
 'any', 'apply', 'basestring', 'bin', 'bool', 'buffer', 'bytearray',
 'bytes', 'callable', 'chr', 'classmethod', 'cmp', 'coerce', 'compile',
 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod',
 'enumerate', 'eval', 'execfile', 'exit', 'file', 'filter', '...'],
 144)

# Let’s explore python __builtins__

explore = [b for b in dir(__builtins__) if not b[0].isupper()
                                            if not b.startswith('_')]
print(explore)
# ['abs',        'all',        'any',         'ascii',        'bin',     'bool',
#  'breakpoint', 'bytearray',
#  'bytes',      'callable',   'chr',         'classmethod',  'compile', 'complex',
#  'copyright',  'credits',    'delattr',     'dict',         'dir',     'display',
#  'divmod',     'enumerate',  'eval',        'exec',         'filter',  'float',
#  'format',     'frozenset',  'get_ipython', 'getattr',      'globals', 'hasattr',
#  'hash',       'help',       'hex',         'id',           'input',   'int',
#  'isinstance', 'issubclass', 'iter',        'len',          'license', 'list',
#  'locals',     'map',        'max',         'memoryview',   'min',     'next',
#  'object',     'oct',        'open',        'ord',          'pow',     'print',
#  'property',   'range',      'repr',        'reversed',     'round',   'set',
#  'setattr',    'slice',      'sorted',      'staticmethod', 'str',     'sum',
#  'super',      'tuple',      'type',        'vars',         'zip']
len(explore)

# a = X if cond1 else Y

name = 'Luis'
long_name = name if len(name) > 8 else False; long_name
# False

name = 'Leopoldine'
long_name = name.upper() if len(name) > 8 else False; long_name
# 'LEOPOLDINE'
for loop

ll
# [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

for i in ll:
    print('.', end=',')
# .,.,.,.,.,.,.,.,.,.,
if, elif, elif, …, else

for i in ll:
    if isinstance(i, int):
        print(f"Yes '{i}' is an int")
    elif isinstance(i, str):
        if i in ['world', 'luis']:
            print(f"String in the list: '{i}' -> ok")
    else:
        print(f"Not an int, not a string: {i} ;( ")
# Yes '22' is an int
# Not an int, not a string: 3.14 ;(
# String: 'world' -> ok
# Yes '3' is an int
# Yes '7' is an int
